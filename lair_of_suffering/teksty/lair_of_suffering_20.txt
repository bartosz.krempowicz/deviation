Lair of Suffering

Constant pain causes monotony
This is the penance for past mistakes
Escaping reality, in which I don't want to live
Will never bring relief again

Lying
Embarrassed
Tortured by the needles of time
Filthy
Spirit
Is not an excuse
Tormented
Conscience
Becomes the tormentor
Slaughtering my inner self

Act is always the same, it cannot be changed
Paralyzed by machine that is your own invention
Needles begin their work, but you can't do anything to stop their craft
Precise process, carving the letters that form several sentences
Your body begins to sweat, but this is not a problem
Content of judgement will be read from your own corpse

Lie down in the lair of suffering

I don't want to wake up
Let me die in my sleep
I don't want to lay down
To feel that pain over and over again

Rusty needles working amongst the dark
Mental suffering turned into physical pain
Imaginary torturer calls me in my head
By my name
Warm refugee for me is torture chamber
No escape from memories
Humanity is a disease
That I'm suffering through

I don't want to feel anything
I don't want to fell jack shit
I don't want to feel anything
I don't want to feel jack shit
Any more
